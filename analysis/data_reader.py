import os
from typing import List, Union

import numpy as np

ROOT = "data/"
TEST_ORDER = ["hpl", "hpcg", "cp2k", "stream", "io"]
GPU_TEST = ["gpu_bandwidth", "gpu_computation"]
TEST_TYPE = ["short", "long"]

ALL_TESTS = TEST_ORDER.copy()
ALL_TESTS.extend(GPU_TEST)


def get_directories(path: str) -> List[str]:
    d = os.listdir(path)
    d = [path + f + "/" for f in d if os.path.isdir(path + f)]
    return d


class DataReader:
    def __init__(self, path: str):
        directories = get_directories(path)

        # Read all data
        self.data = {}
        for directory in directories:
            node_type = self._convert_directory_to_label(directory, path)
            self.data[node_type] = self._read_node_type(directory)

    def _convert_directory_to_label(self, directory: str, path: str) -> str:
        return directory.replace(path, "").replace("/", "")

    def _read_node_type(self, path: str) -> dict:
        data = {}
        directories = get_directories(path)
        for directory in directories:
            node = self._convert_directory_to_label(directory, path)
            data[node] = self._read_node(directory)
        return data

    def _read_node(self, path: str) -> dict:
        data = {}
        for test_type in TEST_TYPE:
            tmp = {}
            for test in ALL_TESTS:
                directory = f"{path}{test_type}/{test}"
                if os.path.isfile(directory):
                    length, labels = DataReader.test_output_info(test)
                    tmp[test] = self._read_file(directory, length, labels)
            data[test_type] = tmp
        return data

    def _can_use_line(self, line: str, length: int):
        """
        Check if the line is correct or not
        """
        line = line.split(" ")
        if len(line) != length:
            return False

        for el in line:
            if len(el) == 0:
                return False

        return True

    def _read_file(self, path: str, length: int, labels: List[str]) -> np.array:
        with open(path, "r") as f:
            data = f.read().split("\n")

        output = []
        for d in data:
            # Remove empty line
            if not self._can_use_line(d, length):
                continue

            # Split the elements
            d = d.split(" ")

            d[0] = np.datetime64(d[0], "m")
            for i in range(1, length):
                d[i] = float(d[i])
            output.append(tuple(d))

        data = output
        dtype = [("date", "datetime64[m]")]
        for i in range(length - 1):
            dtype.append((labels[i], np.float32))
        dtype = np.dtype(dtype)
        data = np.array(data, dtype=dtype)
        data = np.sort(data)
        return data

    def _get_data_node_type(
        self,
        test: str,
        test_type: str,
        node_type: str,
        node: Union[str, None],
        aggregate_nodes: bool = True,
    ) -> List:
        if node is not None and node not in self.data[node_type]:
            return []

        if aggregate_nodes:
            output = []
        else:
            output = {}

        # Get all nodes
        nodes = self.get_nodes(node_type) if node is None else [node]
        for node in nodes:
            node_data = self.data[node_type][node]
            if test_type not in node_data:
                continue
            if test not in node_data[test_type]:
                continue

            if aggregate_nodes:
                output.extend(node_data[test_type][test])
            else:
                output[node] = node_data[test_type][test]

        if aggregate_nodes:
            return np.array(output)
        else:
            return output

    def get_node_types(self) -> List[str]:
        return list(self.data.keys())

    def get_nodes(self, node_type: str = None) -> List[str]:
        if node_type is None:
            nodes = []
            for node_type in self.data.keys():
                tmp = self.data[node_type].keys()
                nodes.extend(list(tmp))
        else:
            nodes = self.data[node_type].keys()
        return list(nodes)

    def get_data(
        self,
        test: str,
        test_type: str,
        node_type: str = None,
        node: str = None,
        aggregate_nodes: bool = True,
    ) -> np.array:
        if test not in ALL_TESTS:
            raise Exception(f"Unknown test {test}")
        if test_type not in TEST_TYPE:
            raise Exception(f"Unknown test type {test_type}")

        output = {}
        node_types = self.get_node_types() if node_type is None else [node_type]
        for node_type in node_types:
            tmp = self._get_data_node_type(
                test, test_type, node_type, node, aggregate_nodes
            )
            if len(tmp) == 0:
                continue

            output[node_type] = tmp

        return output

    @staticmethod
    def test_output_info(test: str) -> int:
        """
        return the number of columns in a given output file and the name of the columns
        """
        # Don't need to return the label for the date. This is common to all tests
        if test == "gpu_bandwidth":
            return 4, ["bandwidth_h2d", "bandwidth_d2h", "bandwidth_d2d"]
        elif test == "cp2k":
            return 2, ["performances"]
        elif test in ["stream", "io"]:
            return 2, ["bandwidth"]
        else:
            return 2, ["gflops"]

    @staticmethod
    def getUnit(test: str) -> str:
        # Don't need to return the label for the date. This is common to all tests
        if test == "gpu_bandwidth":
            return "GB/s"
        elif test == "cp2k":
            return "1/s"
        elif test == "stream":
            return "MB/s"
        elif test == "io":
            return "GB/s"
        else:
            return "GFlop/s"
