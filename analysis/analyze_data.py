import sys
from enum import Enum

import matplotlib.pyplot as plt
import numpy as np
from data_reader import ALL_TESTS, ROOT, TEST_TYPE, DataReader
from matplotlib.backends.backend_qtagg import FigureCanvas
from matplotlib.backends.backend_qtagg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.qt_compat import QtWidgets
from matplotlib.figure import Figure

ALL = "All"
LEGEND_FONTSIZE = 6


class GraphType(Enum):
    HISTOGRAM = "Histogram"
    TIME_EVOLUTION = "Time Evolution"


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self, reader):
        super().__init__()
        self.reader = reader

        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        layout = QtWidgets.QVBoxLayout(self._main)

        # Add options
        self.create_options(layout)

        self.static_canvas = FigureCanvas(Figure())
        self.static_canvas.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Expanding,
            QtWidgets.QSizePolicy.Policy.Expanding,
        )
        # Ideally one would use self.addToolBar here, but it is slightly
        # incompatible between PyQt6 and other bindings, so we just add the
        # toolbar as a plain widget instead.
        layout.addWidget(self.static_canvas)
        layout.addWidget(NavigationToolbar(self.static_canvas, self))

        # Render the graph
        self._update_canvas()

    def create_options(self, layout: QtWidgets.QLayout):
        # Create layout
        layout_options = QtWidgets.QGridLayout()
        layout.addLayout(layout_options)

        # Create node types
        label = QtWidgets.QLabel("Node Type")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 0, 0)

        self.combo_node_types = QtWidgets.QComboBox(self._main)
        self.combo_node_types.addItem(ALL)
        for node_types in sorted(self.reader.get_node_types()):
            self.combo_node_types.addItem(node_types)
        layout_options.addWidget(self.combo_node_types, 1, 0)

        self.combo_node_types.currentIndexChanged.connect(self.update_nodes)
        self.combo_node_types.currentIndexChanged.connect(self._update_canvas)

        # Create nodes
        label = QtWidgets.QLabel("Node")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 0, 1)

        self.combo_nodes = QtWidgets.QComboBox(self._main)
        self.update_nodes(init=True)
        layout_options.addWidget(self.combo_nodes, 1, 1)

        self.combo_nodes.currentIndexChanged.connect(self._update_canvas)

        # Create tests
        label = QtWidgets.QLabel("Test")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 2, 0)

        self.combo_tests = QtWidgets.QComboBox(self._main)
        for test in sorted(ALL_TESTS):
            self.combo_tests.addItem(test)
        layout_options.addWidget(self.combo_tests, 3, 0)

        self.combo_tests.currentIndexChanged.connect(self._update_canvas)

        # Create test type
        label = QtWidgets.QLabel("Test Type")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 2, 1)

        self.combo_test_type = QtWidgets.QComboBox(self._main)
        for test in TEST_TYPE:
            self.combo_test_type.addItem(test)
        layout_options.addWidget(self.combo_test_type, 3, 1)

        self.combo_test_type.currentIndexChanged.connect(self._update_canvas)

        # Create test type
        label = QtWidgets.QLabel("Graph Type")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 4, 0)

        self.combo_graph_type = QtWidgets.QComboBox(self._main)
        for test in GraphType:
            self.combo_graph_type.addItem(test.value)
        layout_options.addWidget(self.combo_graph_type, 5, 0)

        self.combo_graph_type.currentIndexChanged.connect(self._update_canvas)

        # Number of bins
        label = QtWidgets.QLabel("Number of Bins")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 4, 1)

        self.spin_number_bins = QtWidgets.QSpinBox(self._main)
        self.spin_number_bins.setMinimum(2)
        self.spin_number_bins.setMaximum(100000)
        self.spin_number_bins.setSingleStep(5)
        self.spin_number_bins.setValue(20)
        layout_options.addWidget(self.spin_number_bins, 5, 1)

        self.spin_number_bins.valueChanged.connect(self._update_canvas)

        # Number of bins
        label = QtWidgets.QLabel("Number of Tests")
        label.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Preferred,
            QtWidgets.QSizePolicy.Policy.Preferred,
        )
        layout_options.addWidget(label, 6, 0)

        self.label_number_tests = QtWidgets.QLabel("0")
        layout_options.addWidget(self.label_number_tests, 7, 0)

    def get_current_node_type(self):
        node_type = self.combo_node_types.currentText()
        if node_type == ALL:
            return None
        else:
            return node_type

    def get_current_node(self):
        node = self.combo_nodes.currentText()
        if node == ALL:
            return None
        else:
            return node

    def update_nodes(self, init=False):
        current = self.combo_nodes.currentText()

        # Block signal
        self.combo_nodes.blockSignals(True)

        # Remove everything
        self.combo_nodes.clear()

        # Add all the nodes
        self.combo_nodes.addItem(ALL)
        node_type = self.get_current_node_type()
        for node in sorted(self.reader.get_nodes(node_type)):
            self.combo_nodes.addItem(node)

        # Update the index to keep the same value
        ind = self.combo_nodes.findText(current)
        if ind != -1:
            self.combo_nodes.setCurrentIndex(ind)

        # Unblock signal
        self.combo_nodes.blockSignals(False)

    def plot_histogram(self, data, test):
        jet = plt.get_cmap("coolwarm")
        scale, labels = DataReader.test_output_info(test)
        scale -= 1  # Don't count date
        colors = iter(jet(np.linspace(0, 1, scale * len(data))))

        ax = self.static_canvas.figure.subplots()
        for node_type, value in data.items():
            for name in value.dtype.names[1:]:
                if len(value[name]) < 5:
                    continue
                bins = self.spin_number_bins.value()
                color = next(colors)

                if len(data) != 1:
                    label = f"{name}-{node_type}"
                else:
                    label = name

                print(f"Mean value: {value[name].mean()}")
                ax.hist(value[name], bins=bins, density=True, alpha=0.5, color=color)
                ax.hist(
                    value[name],
                    bins=bins,
                    histtype="step",
                    label=label,
                    density=True,
                    color=color,
                )

        ax.legend(fontsize=LEGEND_FONTSIZE)
        unit = DataReader.getUnit(test)
        ax.set_xlabel(f"Target [{unit}]")
        ax.set_ylabel("Density")

    def plot_time_evolution(self, data, test, plot_node):
        if plot_node:
            jet = plt.get_cmap("gnuplot")
            scale, labels = DataReader.test_output_info(test)
            scale -= 1  # Don't count date
            colors = iter(
                jet(np.linspace(0, 1, scale * sum([len(d) for d in data.values()])))
            )

        ax = self.static_canvas.figure.subplots()
        for node_type, node_type_value in data.items():
            # Normal case
            if plot_node:
                for node, value in sorted(node_type_value.items()):
                    for name in value.dtype.names[1:]:
                        color = next(colors)
                        value = np.sort(value)
                        ax.plot(value["date"], value[name], color=color, label=node)
            # For all nodes, we don't want to have one line per node
            else:
                for name in node_type_value.dtype.names[1:]:
                    value = np.sort(node_type_value)
                    ax.plot(value["date"], value[name], label=node_type)

        ax.legend(fontsize=LEGEND_FONTSIZE)
        ax.set_xlabel("Date")
        unit = DataReader.getUnit(test)
        ax.set_ylabel(f"Target [{unit}]")

    def _update_canvas(self):
        # Get the options
        node_type = self.get_current_node_type()
        node = self.get_current_node()
        test = self.combo_tests.currentText()
        test_type = self.combo_test_type.currentText()
        graph_type = self.combo_graph_type.currentText()

        aggregate_nodes = graph_type == GraphType.HISTOGRAM.value
        aggregate_nodes = aggregate_nodes or (
            graph_type == GraphType.TIME_EVOLUTION.value and node_type is None
        )
        data = self.reader.get_data(
            test, test_type, node_type, node, aggregate_nodes=aggregate_nodes
        )

        # Update counter
        if aggregate_nodes:
            number = sum([len(d) for d in data.values()])
        else:
            number = 0
            for d in data.values():
                number += sum([len(tmp) for tmp in d.values()])
        self.label_number_tests.setText(str(number))

        # Warn user than we cannot find any data
        if len(data) == 0:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Icon.Critical)
            msg.setText("Cannot find any data")
            msg.setWindowTitle("Error")
            msg.exec()

        # Do the plot
        self.static_canvas.figure.clf()
        if graph_type == GraphType.HISTOGRAM.value:
            self.plot_histogram(data, test)
        elif graph_type == GraphType.TIME_EVOLUTION.value:
            self.plot_time_evolution(data, test, plot_node=not aggregate_nodes)

        self.static_canvas.draw()


if __name__ == "__main__":
    # Check whether there is already a running QApplication (e.g., if running
    # from an IDE).
    qapp = QtWidgets.QApplication.instance()
    if not qapp:
        qapp = QtWidgets.QApplication(sys.argv)

    reader = DataReader(ROOT)
    app = ApplicationWindow(reader)
    app.show()
    app.activateWindow()
    app.raise_()
    qapp.exec()
