import argparse
import os
from copy import deepcopy
from datetime import datetime, timedelta

import numpy as np

ROOT = "data/errors"
TESTS_DTYPE = {
    "cp2k": [("score", np.float32)],
    "gpu_bandwidth": [("h2d", np.float32), ("d2h", np.float32), ("d2d", np.float32)],
    "gpu_computation": [("score", np.float32)],
    "hpcg": [("score"), np.float32],
    "hpl": [("score", np.float32)],
    "stream": [("score", np.float32)],
    "io": [("score", np.float32)],
}

dtype = [("node", "U30"), ("date", "M8[m]")]


def read_file(filename, days, test):
    dt = deepcopy(dtype)
    dt.extend(TESTS_DTYPE[test])

    limit = datetime.now() - timedelta(days=days)

    data = np.loadtxt(filename, dtype=dt)
    ind = data["date"] >= limit
    return data[ind]


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Find node failures from the error files"
    )
    parser.add_argument(
        "--days",
        dest="days",
        default=180,
        help="Number of days on which to look for failure",
    )
    return parser.parse_args()


def main():
    args = parse_arguments()
    tests = os.listdir(ROOT)

    data = {}
    nodes = []
    for test in tests:
        test_name = test.split(".")[0]
        data[test_name] = read_file(f"{ROOT}/{test}", args.days, test_name)
        nodes.extend(data[test_name]["node"])

    print("The following errors were reported in the error files:")
    print()
    node_text = "Node"
    title = f"|{node_text:^15}|"
    for key in sorted(data.keys()):
        title += f"{key:^15}|"
    print(title)

    nodes = np.unique(nodes)
    for node in nodes:
        text = f"|{node:^15}|"
        for key, value in sorted(data.items(), key=lambda x: x[0]):
            failures = np.sum(data[key]["node"] == node)
            failures = str(failures)
            text += f"{failures:^15}|"

        print(text)


if __name__ == "__main__":
    main()
