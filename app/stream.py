from statistics import median

from abstract_test import AbstractTest
from utils import get_number_cores, run_command


class StreamTest(AbstractTest):
    def get_test_name(self):
        return "stream"

    def _run_test(self, step: int) -> float:
        values = []
        for i in range(5):
            # Run test
            stdout, _ = run_command(["stream"])
            score = [line for line in stdout.split("\n") if "Triad" in line][0]
            score = float(score.split()[1])
            values.append(score)

        return median(values)

    def get_number_tests(self):
        return 1

    def get_units(self):
        return "MB/s"

    def get_number_omp_threads(self):
        return get_number_cores()
