import logging
import multiprocessing
import os
import subprocess
from enum import IntEnum
from functools import lru_cache
from typing import List, Union

import psutil
from gpustat.core import GPUStatCollection
from pyrsmi import rocml

MEMORY_ROUNDING_GIB = 16

BLACKLIST_GPUS = ["0x1002"]
BLACKLIST_GPUS = [gpu.replace(" ", "_") for gpu in BLACKLIST_GPUS]


class GPUType(IntEnum):
    AMD = 1
    NVIDIA = 2


@lru_cache
def get_host_type():
    # CPU
    with open("/proc/cpuinfo", "r") as f:
        for line in f.readlines():
            if "model name" in line:
                line = line.split(":")[-1]
                line = line.strip()
                cpu = line.replace(" ", "_")

    # Memory
    mem_bytes = os.sysconf("SC_PAGE_SIZE") * os.sysconf("SC_PHYS_PAGES")
    mem_gib = int(mem_bytes / (1024**3 * MEMORY_ROUNDING_GIB))
    mem_gib *= MEMORY_ROUNDING_GIB
    mem_gib = f"{mem_gib}GiB"

    # GPU
    gpus = get_number_gpus()
    if gpus > 0:
        gpu_name = get_gpu_name()
        return f"{cpu},{mem_gib},{gpu_name}"
    else:
        return f"{cpu},{mem_gib}"


def results_to_string(values: Union[List[float], float]) -> str:
    if not isinstance(values, list):
        return str(values)
    else:
        values = [str(val) for val in values]
        return " ".join(values)


@lru_cache
def get_gpu_name() -> str:
    # Assume all GPUs are the same
    data = None
    try:
        data = GPUStatCollection.new_query()[0].name
    except Exception:
        rocml.smi_initialize()
        data = rocml.smi_get_device_name(0)
        rocml.smi_shutdown()
    return data.replace(" ", "_")


@lru_cache
def get_gpu_type() -> GPUType:
    try:
        rocml.smi_initialize()
        rocml.smi_shutdown()
        return GPUType.AMD
    except Exception:
        return GPUType.NVIDIA


@lru_cache
def get_number_gpus() -> int:
    # GPU
    try:
        data = GPUStatCollection.new_query()
        return len(data.gpus)
    except Exception:
        try:
            rocml.smi_initialize()
            number = rocml.smi_get_device_count()
            rocml.smi_shutdown()

            # Blacklist some GPUs
            name = get_gpu_name()
            if name in BLACKLIST_GPUS:
                number = 0
            return number
        except Exception:
            return 0


@lru_cache
def get_number_cores():
    return multiprocessing.cpu_count()


@lru_cache
def get_total_ram():
    return psutil.virtual_memory().total


def run_command(command: List[str], path: str = None, env=None):
    try:
        if env is not None:
            my_env = os.environ.copy()
            my_env.update(env)
            env = my_env
        proc = subprocess.run(
            command, capture_output=True, check=True, text=True, cwd=path, env=env
        )  # nosec
    except subprocess.CalledProcessError as e:
        logging.exception(e)
        raise Exception(f"stderr: {e.stderr}")
    return proc.stdout, proc.stderr


if __name__ == "__main__":
    print(get_host_type())
