from tempfile import TemporaryDirectory

from abstract_test import AbstractTest
from utils import run_command


class HPCGTest(AbstractTest):
    def get_test_name(self) -> str:
        return "hpcg"

    def _run_test(self, step: int) -> float:
        with TemporaryDirectory() as directory:
            # Generate input
            filename = directory + "/hpcg.dat"
            with open(filename, "w") as f:
                f.write("HPCG benchmark input file\n")
                f.write(
                    "Sandia National Laboratories; University of Tennessee, Knoxville\n"
                )
                if self.run_short_test:
                    f.write("32 32 32\n")
                    f.write("30")
                else:
                    f.write("128 128 128\n")
                    f.write("300")

            # Run hpcg
            stdout, _ = run_command(
                [
                    "mpirun",
                    "--allow-run-as-root",
                    "--bind-to-core",
                    "--map-by",
                    "core",
                    "xhpcg",
                ],
                path=directory,
            )
            stdout = [line for line in stdout.split("\n") if "Report" in line]
            report = stdout[0].split()[-1]

            # Get results
            with open(f"{directory}/{report}", "r") as f:
                lines = f.readlines()
                score = [line for line in lines if "VALID" in line]
                score = score[0].split()[-1].rstrip()
                score = float(score.split("=")[-1])

        return score

    def get_number_tests(self):
        return 1

    def get_units(self):
        return "GFlop/s"

    def get_number_omp_threads(self):
        return 1
