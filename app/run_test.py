import argparse
import logging

from cp2k import CP2KTest
from gpu_bandwidth import GPUBandwidthTest
from gpu_computation import GPUComputationTest
from hpcg import HPCGTest
from hpl import HPLTest
from io_test import IOTest
from stream import StreamTest
from utils import get_host_type, get_number_gpus

FORMAT = "%(levelname)s:     %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)


def parse_arguments():
    parser = argparse.ArgumentParser(
        prog="HPC Benchmarks",
        description="This application runs HPC benchmarks and "
        "checks against fixed values the results",
    )

    parser.add_argument(
        "-n",
        "--no-failure",
        dest="no_failure",
        action="store_true",
        help="The code will continue even if a test fails",
    )
    parser.add_argument("--cp2k", dest="cp2k", action="store_true", help="Run cp2k")
    parser.add_argument("--hpl", dest="hpl", action="store_true", help="Run hpl")
    parser.add_argument("--hpcg", dest="hpcg", action="store_true", help="Run hpcg")
    parser.add_argument(
        "--stream", dest="stream", action="store_true", help="Run stream"
    )
    parser.add_argument(
        "--gpu-bandwidth",
        dest="gpu_bandwidth",
        action="store_true",
        help="Run GPU bandwdith",
    )
    parser.add_argument(
        "--gpu-computation",
        dest="gpu_computation",
        action="store_true",
        help="Run GPU computation",
    )
    parser.add_argument("--io", dest="io", action="store_true", help="Run IO")
    parser.add_argument(
        "--all", dest="all_tests", action="store_true", help="Run all benchmarks"
    )
    parser.add_argument(
        "--get-spec",
        dest="get_spec",
        action="store_true",
        help="Provide the spec and exit",
    )
    parser.add_argument(
        "--long-tests",
        dest="long_tests",
        action="store_true",
        help="Run the long test",
    )
    parser.add_argument(
        "--target-directory",
        dest="target_directory",
        default="/targets",
        help="Directory in which to look for the target values",
    )
    args = parser.parse_args()

    if True not in [
        args.cp2k,
        args.hpl,
        args.hpcg,
        args.stream,
        args.gpu_bandwidth,
        args.gpu_computation,
        args.io,
    ]:
        args.all_tests = True

    return args


def hpl(args):
    test = HPLTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


def hpcg(args):
    test = HPCGTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


def cp2k(args):
    test = CP2KTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


def stream(args):
    test = StreamTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


def gpu_bandwidth(args):
    test = GPUBandwidthTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


def gpu_computation(args):
    test = GPUComputationTest(
        args.target_directory, args.no_failure, not args.long_tests
    )
    test.run_test()


def io(args):
    test = IOTest(args.target_directory, args.no_failure, not args.long_tests)
    test.run_test()


if __name__ == "__main__":
    args = parse_arguments()
    logging.info(f"Machine: {get_host_type()}")
    if args.get_spec:
        exit(0)

    logging.info(f"Running the short tests: {not args.long_tests}")

    if args.hpl or args.all_tests:
        hpl(args)
    if args.hpcg or args.all_tests:
        hpcg(args)
    if args.cp2k or args.all_tests:
        cp2k(args)
    if args.stream or args.all_tests:
        stream(args)
    if args.io or args.all_tests:
        io(args)

    if get_number_gpus() > 0:
        if args.gpu_bandwidth or args.all_tests:
            gpu_bandwidth(args)
        if args.gpu_computation or args.all_tests:
            gpu_computation(args)

    logging.info("All the requested benchmarks are done")
