import tempfile

from abstract_test import AbstractTest
from utils import run_command


class IOTest(AbstractTest):
    def get_test_name(self):
        return "io"

    def _run_test(self, step: int) -> float:
        filesize = 2
        with tempfile.NamedTemporaryFile(prefix="hpc-bench-io-test.") as fp:
            filename = fp.name
            _, stderr = run_command(
                [
                    "dd",
                    "if=/dev/zero",
                    f"of={filename}",
                    f"bs={filesize}G",
                    "count=1",
                    "conv=fdatasync",
                ]
            )
            stderr = [line for line in stderr.split("\n") if "copied" in line]
            time = float(stderr[0].split()[-4])
            return 1000 * filesize / time

    def get_number_tests(self):
        return 1

    def get_units(self):
        return "GB/s"

    def get_number_omp_threads(self):
        return None
