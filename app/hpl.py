import math
from tempfile import TemporaryDirectory

from abstract_test import AbstractTest
from utils import get_number_cores, get_total_ram, run_command


class HPLTest(AbstractTest):
    def get_test_name(self) -> str:
        return "hpl"

    def get_parameters(self, num_threads):
        # Implementation from
        # http://www.advancedclustering.com/act-kb/tune-hpl-dat-file/
        total_cores = get_number_cores() // num_threads
        sqrt_num = int(math.sqrt(total_cores))
        possible = []

        for i in range(2, sqrt_num + 1):
            if total_cores % i == 0:
                possible.append(i)

        if len(possible) == 0:
            possible.append(1)

        P = 0
        diff = 0
        for i in range(0, len(possible)):
            if diff == 0:
                diff = total_cores - possible[i]
            if P == 0:
                P = possible[i]
            tmpDiff = total_cores - possible[i]
            if tmpDiff < diff:
                diff = tmpDiff
                P = possible[i]

        Q = int(total_cores / P)

        # Now compute N
        mem_frac = 0.2 if self.run_short_test else 0.8
        upper_limit_gb = 1024
        mem_total = min(upper_limit_gb * 1024**3, get_total_ram())
        N = int(math.sqrt(mem_total / 8) * mem_frac)

        return N, P, Q

    def generate_input(self, f, num_threads):
        nsize, psize, qsize = self.get_parameters(num_threads)
        f.write("HPLinpack benchmark input file\n")
        f.write("Innovative Computing Laboratory, University of Tennessee\n")
        f.write("HPL.out      output file name (if any)\n")
        f.write("6            device out (6=stdout,7=stderr,file)\n")
        f.write("1            # of problems sizes (N)\n")
        f.write(f"{nsize}         Ns\n")
        f.write("1            # of NBs\n")
        f.write("192           NBs\n")
        f.write("0            PMAP process mapping (0=Row-,1=Column-major)\n")
        f.write("1            # of process grids (P x Q)\n")
        f.write(f"{psize}         Ps\n")
        f.write(f"{qsize}         Qs\n")
        f.write("16.0         threshold\n")
        f.write("1            # of panel fact\n")
        f.write("2            PFACTs (0=left, 1=Crout, 2=Right)\n")
        f.write("1            # of recursive stopping criterium\n")
        f.write("4            NBMINs (>= 1)\n")
        f.write("1            # of panels in recursion\n")
        f.write("2            NDIVs\n")
        f.write("1            # of recursive panel fact.\n")
        f.write("1            RFACTs (0=left, 1=Crout, 2=Right)\n")
        f.write("1            # of broadcast\n")
        f.write("1            BCASTs (0=1rg,1=1rM,2=2rg,3=2rM,4=Lng,5=LnM)\n")
        f.write("1            # of lookahead depth\n")
        f.write("1            DEPTHs (>=0)\n")
        f.write("2            SWAP (0=bin-exch,1=long,2=mix)\n")
        f.write("64           swapping threshold\n")
        f.write("0            L1 in (0=transposed,1=no-transposed) form\n")
        f.write("0            U  in (0=transposed,1=no-transposed) form\n")
        f.write("1            Equilibration (0=no,1=yes)\n")
        f.write("8            memory alignment in double (> 0)")

    def _run_test(self, step: int) -> float:
        num_threads = self.get_number_omp_threads()
        num_tasks = get_number_cores() // num_threads

        with TemporaryDirectory() as directory:
            with open(directory + "/HPL.dat", "w") as f:
                self.generate_input(f, num_threads)

            stdout, _ = run_command(
                [
                    "mpirun",
                    "--allow-run-as-root",
                    "-n",
                    str(num_tasks),
                    "--bind-to",
                    "l3cache",
                    "xhpl",
                ],
                path=directory,
            )

            stdout = [line for line in stdout.split("\n") if line.startswith("WR")]
            score = stdout[0].split()[-1]
        return float(score)

    def get_number_tests(self):
        return 1

    def get_units(self):
        return "GFlop/s"

    def get_number_omp_threads(self):
        return 4
