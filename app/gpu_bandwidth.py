from functools import lru_cache
from statistics import median

from abstract_test import AbstractTest
from utils import GPUType, get_gpu_type, get_number_gpus, run_command


class GPUBandwidthTest(AbstractTest):
    def get_test_name(self):
        return "gpu_bandwidth"

    def _run_test(self, step):
        gpu = get_gpu_type()
        if gpu == GPUType.AMD:
            return self.run_amd(step)
        else:
            return self.run_nvidia(step)

    @lru_cache
    def amd_get_ids(self):
        cpus = []
        gpus = []

        stdout, _ = run_command(["bandwidthTest-amd", "-e"])
        stdout = stdout.split("\n\n")
        devices = [out.split("\n") for out in stdout if "Device" in out]

        for device in devices:
            device = [d for d in device if len(d.strip()) != 0]
            index = int(device[0].split(":")[-1].strip())
            device_type = device[1].split(":")[-1].strip()
            if device_type == "GPU":
                gpus.append(index)
            elif device_type == "CPU":
                cpus.append(index)
            else:
                raise Exception(f"Cannot extract device {device}")

        if len(gpus) != self.get_number_tests():
            raise Exception(f"Cannot find the right number of GPUs: {devices}")

        return {"cpus": cpus, "gpus": gpus}

    def amd_get_result(self, stdout, data_size):
        stdout = stdout.split("\n")
        results = [out for out in stdout if data_size in out and "Launch" not in out]
        # First "column" has 2 subcolumns (value + unit)
        results = [float(res.split()[3]) for res in results]
        return median(results)

    def run_amd(self, step):
        ids = self.amd_get_ids()
        cpus = ",".join([str(i) for i in ids["cpus"]])
        gpus = ids["gpus"]
        all_gpus = ",".join([str(g) for g in gpus])
        current_gpu = str(gpus[step])

        # host to device
        data_size = "512"
        stdout, _ = run_command(
            [
                "bandwidthTest-amd",
                "-s",
                cpus,
                "-d",
                current_gpu,
                "-m",
                data_size,
            ]
        )
        h2d = self.amd_get_result(stdout, data_size)

        # device to host
        stdout, _ = run_command(
            [
                "bandwidthTest-amd",
                "-s",
                current_gpu,
                "-d",
                cpus,
                "-m",
                data_size,
            ]
        )
        d2h = self.amd_get_result(stdout, data_size)

        # device to device
        stdout1, _ = run_command(
            [
                "bandwidthTest-amd",
                "-s",
                all_gpus,
                "-d",
                current_gpu,
                "-m",
                data_size,
            ]
        )
        stdout2, _ = run_command(
            [
                "bandwidthTest-amd",
                "-s",
                current_gpu,
                "-d",
                all_gpus,
                "-m",
                data_size,
            ]
        )
        d2d = self.amd_get_result(stdout1 + "\n" + stdout2, data_size)

        return [h2d, d2h, d2d]

    def run_nvidia(self, step):
        stdout, _ = run_command(
            ["bandwidthTest", f"-device={step}", "-mode=quick", "-csv"]
        )
        stdout = stdout.split("\n")

        h2d = [line for line in stdout if "H2D" in line]
        h2d = float(h2d[0].split()[3])

        d2h = [line for line in stdout if "D2H" in line]
        d2h = float(d2h[0].split()[3])

        d2d = [line for line in stdout if "D2D" in line]
        d2d = float(d2d[0].split()[3])
        return [h2d, d2h, d2d]

    def get_number_tests(self):
        return get_number_gpus()

    def get_units(self):
        return "[H2D, D2H, D2D] GB/s"

    def get_number_omp_threads(self):
        return None
