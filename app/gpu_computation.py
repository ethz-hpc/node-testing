from statistics import median

from abstract_test import AbstractTest
from utils import GPUType, get_gpu_type, get_number_gpus, run_command


class GPUComputationTest(AbstractTest):
    def get_test_name(self):
        return "gpu_computation"

    def _run_test(self, step):
        particles = 1000000
        if not self.run_short_test:
            particles *= 2
        gpu = get_gpu_type()
        if gpu == GPUType.AMD:
            # AMD test is a bit too fast => increase number particles
            data = []
            for i in range(3):
                data.append(self.run_amd(step, 100 * particles))
            return median(data)
        else:
            return self.run_nvidia(step, particles)

    def run_amd(self, gpu_id, particles):
        stdout, _ = run_command(
            ["nbody-amd", str(particles)], env={"ROCR_VISIBLE_DEVICES": str(gpu_id)}
        )
        g_interactions_per_s = float(stdout.strip().split()[-1])
        # Magic number from nvidia
        return g_interactions_per_s

    def run_nvidia(self, gpu_id, particles):
        stdout, _ = run_command(
            ["nbody", "-benchmark", f"-device={gpu_id}", f"-numbodies={particles}"]
        )
        gflops = [line for line in stdout.split("\n") if "GFLOP" in line]
        gflops = float(gflops[0].split()[1])
        return gflops

    def get_number_tests(self):
        return get_number_gpus()

    def get_units(self):
        gpu = get_gpu_type()
        if gpu == GPUType.AMD:
            return "G interactions/s"
        else:
            return "GFlop/s"

    def get_number_omp_threads(self):
        return None

    def run_in_parallel(self) -> bool:
        return True
