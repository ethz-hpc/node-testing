import logging
import os
import smtplib
import socket
from datetime import datetime
from email.message import EmailMessage
from multiprocessing import Array, Lock, Process
from time import sleep
from typing import List, Union

import yaml
from utils import get_host_type, results_to_string


class UnsufficientTest(Exception):
    pass


class AbstractTest:
    hostname = socket.gethostname()
    host_type = get_host_type()

    def __init__(self, directory: str, no_failure: bool, run_short_test: bool):
        self.test = self.get_test_name()
        self.directory = directory
        self.targets = self.get_target_values()
        self.run_short_test = run_short_test
        self.no_failure = no_failure
        self.file_lock = Lock()

    def get_date(self) -> str:
        return datetime.now().isoformat(timespec="minutes")

    def write_error(self, values):
        directory = self.directory + "/errors"
        os.makedirs(directory, exist_ok=True)
        with open(directory + f"/{self.test}.txt", "a") as f:
            date = self.get_date()
            results = results_to_string(values)
            f.write(f"{self.hostname} {date} {results}\n")

    def get_target_values(self):
        filename = self.directory + "/targets.yaml"
        with open(filename) as f:
            data = yaml.safe_load(f)

        if self.hostname in data:
            return data[self.hostname][self.test]

        if self.host_type not in data:
            raise Exception(f"Missing target values for {self.host_type}")

        if self.test not in data[self.host_type]:
            raise Exception(f"Target {self.host_type} is missing test {self.test}")
        return data[self.host_type][self.test]

    def get_email_receivers(self):
        with open(self.directory + "/emails.txt", "r") as f:
            recv = f.read()
        recv = recv.splitlines()

        def filter_lines(line: str) -> bool:
            return len(line) != 0 and line[0] != "#"

        recv = filter(filter_lines, recv)
        return ", ".join(recv)

    def send_email(self):
        msg = EmailMessage()
        msg.set_content(f"Failure on {self.hostname} for {self.test}")
        msg["Subject"] = f"HPC-Benchmarks: Failure on {self.hostname} - {self.test}"
        msg["From"] = "hpc.benchmarks"
        msg["To"] = self.get_email_receivers()

        with smtplib.SMTP("localhost", timeout=60) as s:
            s.send_message(msg)

    def write_results(self, results):
        if self.run_short_test:
            test_type = "short"
        else:
            test_type = "long"

        directory = self.directory + f"/{self.host_type}/{self.hostname}/{test_type}"
        os.makedirs(directory, exist_ok=True)
        with self.file_lock, open(directory + f"/{self.test}", "a") as f:
            date = self.get_date()
            results = results_to_string(results)
            f.write(f"{date} {results}\n")

    def manage_error(self, values):
        failure = (
            f"Test {self.test} was not sufficient for node of type {self.host_type}: "
        )
        failure += f"Expecting {self.targets}, got {values}."
        with self.file_lock:
            self.write_error(values)
        raise UnsufficientTest(failure)

    def check_results(self, values):
        if not isinstance(self.targets, list):
            if float(values) < float(self.targets):
                self.manage_error(values)
            return

        if len(values) != len(self.targets):
            text = "You need to specify the correct number of elements"
            text += f" ({values} vs {self.targets})"
            raise Exception(text)

        for i, val in enumerate(values):
            if float(val) < float(self.targets[i]):
                self.manage_error(values)

    def setup_omp(self):
        num_threads = self.get_number_omp_threads()
        if num_threads is not None:
            os.environ["OMP_NUM_THREADS"] = str(num_threads)

    def cleanup_omp(self):
        if "OMP_NUM_THREADS" in os.environ:
            del os.environ["OMP_NUM_THREADS"]

    def run_test_internal(self, step: int, n_tests: int) -> bool:
        success = False
        for i in range(2):
            try:
                values = self._run_test(step)

                if n_tests != 1:
                    test = f"{self.test} {step}"
                else:
                    test = self.test

                logging.info(f"{test}: {values} {self.get_units()}")
                self.write_results(values)
                self.check_results(values)
                success = True
                break
            except Exception as e:
                if not isinstance(e, UnsufficientTest):
                    logging.exception(e)
                logging.error(e.args[0])
                # Give a small break to avoid correlations between tests
                sleep(1)
        return success

    def raise_error(self):
        self.cleanup_omp()
        self.send_email()
        raise Exception("Failed to run the test")

    def run_test(self):
        logging.info("")
        logging.info(f"Starting to run {self.test}")
        n_tests = self.get_number_tests()

        self.setup_omp()

        if self.run_in_parallel():

            # Handle the output with a multiprocessing object
            successes = Array("b", range(n_tests))

            def f(i):
                successes[i] = self.run_test_internal(i, n_tests)

            # Run the processes
            proc = [Process(target=f, args=(i,)) for i in range(n_tests)]
            [p.start() for p in proc]
            [p.join() for p in proc]

            # Convert back to normal list
            successes = [a for a in successes]

            # Ensure that everything is alright
            if not self.no_failure and False in successes:
                self.raise_error()
        else:
            for j in range(n_tests):
                success = self.run_test_internal(j, n_tests)

                if not success and not self.no_failure:
                    self.raise_error()
        self.cleanup_omp()
        logging.info(f"{self.test} is finished")

    def _run_test(self, step: int) -> Union[float, List[float]]:
        """
        This function needs to return the results as an array or a float.
        The step indicate the step number of the test (e.g. GPU number)
        """
        raise Exception("Not implemented")

    def get_test_name(self) -> str:
        """
        This function needs to return the name of the test
        """
        raise Exception("Not implemented")

    def get_number_tests(self) -> int:
        """
        This function returns the number of times a test needs to be called.
        For example with the GPUs, we want to call the test once per GPU.
        """
        raise Exception("Not implemented")

    def get_units(self) -> str:
        """
        This function returns the units of the results
        """
        raise Exception("Not implemented")

    def get_number_omp_threads(self) -> Union[int, None]:
        """
        This function is used to set OMP_NUM_THREADS
        """
        raise Exception("Not implemented")

    def run_in_parallel(self) -> bool:
        """
        Should the test run in parallel (e.g. on all the GPUs at once)?
        """
        return False
