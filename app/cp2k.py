import logging
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from statistics import median
from tempfile import TemporaryDirectory

from abstract_test import AbstractTest
from utils import get_number_cores, run_command


def run_cp2k(i: int, run_short_test: bool):
    if run_short_test:
        input_file = "/data/H2O-32-short.inp"
    else:
        input_file = "/data/h2O-64.inp"

    with TemporaryDirectory() as directory:
        try:
            stdout, _ = run_command(
                ["numactl", f"--physcpubind={i}", "cp2k.popt", "-i", input_file],
                path=directory,
                env={"LD_LIBRARY_PATH": "/usr/lib/x86_64-linux-gnu"},
            )
            stdout = [line for line in stdout.split("\n") if "CP2K  " in line]
            time = float(stdout[0].split()[5])
            return time
        except Exception as e:
            logging.exception(e)
            return None


class CP2KTest(AbstractTest):
    def get_test_name(self):
        return "cp2k"

    def _run_test(self, step: int):
        logging.warning("CP2K might crash but it will not really impact the score.")
        logging.warning("Issue: https://github.com/cp2k/cp2k/issues/2690")
        logging.warning("The memory seems fine, so I have no idea where it comes from")

        with ThreadPoolExecutor(max_workers=get_number_cores()) as pool:
            f = partial(run_cp2k, run_short_test=self.run_short_test)
            res = pool.map(f, range(get_number_cores()))
            res = [r for r in res]
        count = sum([r is None for r in res])
        logging.info(f"cp2k failed for {count} threads")
        res = [r for r in res if r is not None]
        return get_number_cores() * 3600 * 24 / median(res)

    def get_number_tests(self):
        return 1

    def get_units(self):
        return "1/s"

    def get_number_omp_threads(self):
        return 1
