# Node Testing

Contains everything required for testing a node.

## Warning
This repository is a mirror from the ETHZ HPC private git repository (private documentation on branch `euler`). No changes should be applied directly on gitlab.com. For external PRs, please feel free to do them here. We will apply them manually on our repo.

## Mailing list

If you wish to be contacted for under performing nodes, you can add your address to `${BENCHMARK_DIRECTORY}/emails.txt` (see Makefile for default values).

# Using the script on the cluster

With `make deploy-script`, you can deploy a small script in `REMOTE_BIN_DIRECTORY` which will start the image with singularity. You can pass some arguments to it. For example, you can get the spec with `benchmark-node --get-spec` (default script name) or you can run the tests without stopping on failure with `benchmark-node --no-failure`. For more information, please use `--help`.

# Updating the container

The CI/CD pipeline takes care of almost everything. You have to push the change to the master branch. Once the pipeline succeed, you will need to pull the image on the cluster: `make replace-image`.

# Updating target values

You can change the values in `targets/target.yaml` and deploy them with `make deploy-target` (this is not done by the pipeline).
If you need to find the spec of a node, you can run `NODES=XXX make get-node-spec`.

In order to analyze the previous results, you can run `make download-results analyze-data`. You can install the required libraries with `make analyze-data-install`.


# Running test on a node

If you wish to run on a node (or set of node), you can use `NODES=my-node-[1-4] make run-on-node`.
By default, it will run the short tests. For long ones (at reboot, the short ones are used), you need to specify `SHORT_TEST=no NODES=... make ...`.

If you are on a node with all the "slurm" directory mounted, you can also directly call the script `${REMOTE_BIN_DIRECTORY}/benchmark-node` (see Makefile for default value).
If you wish to update the script, use `make deploy-script`.

# Checking the results

All the previous runs are available in `${BENCHMARK_DIRECTORY}` (see Makefile for default value). They are organized by node type, node name, test type (e.g. short / long) and test name (in this order).

For example to check the results of HPL on a given type of nodes:

    [root@my-node ~]# cat ${BENCHMARK_DIRECTORY}/AMD_EPYC_7742_64-Core_Processor,496GiB,8-Quadro_RTX_6000/*/short/hpl
    2023-03-29T09:11 2.5091e+03
    2023-03-29T09:39 2.5888e+03
    2023-03-29T10:22 2.6027e+03
    2023-03-29T10:36 2.6035e+03
    2023-03-29T09:11 2.4930e+03
    2023-03-29T09:39 2.5614e+03
    2023-03-29T10:22 2.5858e+03
    2023-03-29T10:36 2.5371e+03


A script is available that checks all the nodes for failure. It uses the directory `${BENCHMARK_DIRECTORY}/errors` (see Makefile for default value) where insufficient test are recorded.

    make download-results find-failures

## Checking for changes in the image

The file `${BENCHMARK_DIRECTORY}/updates.txt` (see Makefile for default value) contains the list of date when the image changed. It can help to understand why some results changed.
