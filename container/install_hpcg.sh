#!/bin/bash
set -e

tmpdir=$(mktemp -d)
cd $tmpdir

# Copy code
git clone https://github.com/hpcg-benchmark/hpcg.git

# Get the correct version
cd $tmpdir/hpcg/src
git checkout ${HPCG_COMMIT}
git apply /data/hpcg-patch.txt

# Compile
cd $tmpdir/hpcg
CC=mpicc CXX=mpic++ cmake . -DHPCG_ENABLE_MPI=ON -DHPCG_ENABLE_OPENMP=ON -DHPCG_ENABLE_CONTIGUOUS_ARRAYS=ON -DHPCG_ENABLE_CUBIC_RADICAL_SEARCH=ON
make clean
make -j 10

# Copy to bin
cp xhpcg /usr/bin/

# Ensure to cleanup
rm -rf $tmpdir
