#!/bin/bash
set -e
tempdir=$(mktemp -d)
cd ${tempdir}

git clone https://github.com/NVIDIA/cuda-samples.git
cd cuda-samples
git checkout ${CUDA_SAMPLES_VERSION}


# Install bandwidth
cd Samples/bandwidthTest/
# Nvidia
make
cp bandwidthTest /usr/bin/


cd ../../

# Install nbody
cd Samples/nbody
# Nvidia
make
cp nbody /usr/bin/

# Ensure to cleanup
rm -rf $tempdir

