#/bin/bash

set -e
HPL_DIR=$(mktemp -d)
cd ${HPL_DIR}

# Get the code
wget https://netlib.org/benchmark/hpl/hpl-${HPL_VERSION}.tar.gz
tar -xvf hpl-${HPL_VERSION}.tar.gz
cd hpl-${HPL_VERSION}

# Copy the configuration
cp /data/Make.Linux .
sed -i 's@^TOPdir.*@TOPdir = '"${PWD}"'@g' Make.Linux

# Compile code
# Not sure why but need to compile twice
set +e
make arch=Linux
set -e
make arch=Linux

# Install manually
ls bin/Linux/
cp bin/Linux/xhpl /usr/bin/

# Ensure to cleanup
rm -rf ${HPL_DIR}
