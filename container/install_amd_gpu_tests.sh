#!/bin/bash
set -e
tempdir=$(mktemp -d)
cd ${tempdir}


# Install bandwidth
git clone https://github.com/ROCm/rocm_bandwidth_test.git
cd rocm_bandwidth_test
git checkout ${HIP_SAMPLES_VERSION}
cmake . -DCMAKE_BUILD_TYPE=Release
make
cp rocm-bandwidth-test /usr/bin/bandwidthTest-amd


cd ${tempdir}

# Install nbody
git clone https://github.com/ROCm/HIP-Examples.git
cd HIP-Examples
git checkout ${HIP_SAMPLES_VERSION}

cd mini-nbody/hip
hipcc -I../ -DSHMOO nbody-block.cpp -o nbody
cp nbody /usr/bin/nbody-amd

# Ensure to cleanup
rm -rf $tempdir
