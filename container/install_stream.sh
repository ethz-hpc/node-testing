#!/bin/bash

set -e
tempdir=$(mktemp -d)
cd ${tempdir}

# Get the code
git clone https://github.com/jeffhammond/STREAM.git
cd STREAM
git checkout 1a78bcc

# Compile
sed -i 's/\(^CFLAGS.*\)/\1 -DSTREAM_ARRAY_SIZE=430080000 -DNTIMES=100 -mcmodel=medium/g' Makefile
make

# Install
cp stream_c.exe /usr/bin/stream

# Ensure to cleanup
rm -rf $tempdir
