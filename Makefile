.ONESHELL:
VERSION ?= `date +%Y.%m.%d`
DOCKER ?= docker
APPTAINER ?= /usr/bin/apptainer
LOCAL_APPTAINER ?= $(APPTAINER)
DIRECTORY_OWNER ?= $(USER)
REMOTE_BIN_DIRECTORY ?= /usr/bin
REMOTE_BIN_NAME ?= benchmark-node
RANDOM_NODE ?= my-node-01
ADMIN_NODE ?= my-node-01
# The following env should be sync with hpc-benchmarks.service
REGISTRY_NODE ?= registry.gitlab.com/ethz-hpc/node-testing
REGISTRY_LOCAL ?= registry.gitlab.com/ethz-hpc/node-testinge
SHORT_TEST ?= yes
BENCHMARK_DIRECTORY ?= /var/benchmarks
CACHE_DIR=/tmp/.apptainer
APPTAINER_IMAGE=$(BENCHMARK_DIRECTORY)/test.sif

# Container image
build-image:
	$(DOCKER) build --build-arg http_proxy="${http_proxy}" --build-arg https_proxy="${https_proxy}" \
		--build-arg no_proxy="${no_proxy}" $${EXTRA_ARGS} \
		--network host . -t node-testing:latest \
		-f container/Dockerfile

hadolint:
	hadolint container/Dockerfile

push-image:
	echo \\n The image will be pushed to $(REGISTRY_LOCAL). Do you agree?
	read ok

	$(DOCKER) tag node-testing:latest $(REGISTRY_LOCAL)/node-testing:$(VERSION)
	$(DOCKER) push $(REGISTRY_LOCAL)/node-testing:$(VERSION)
	$(DOCKER) tag node-testing:latest $(REGISTRY_LOCAL)/node-testing:latest
	$(DOCKER) push $(REGISTRY_LOCAL)/node-testing:latest
	echo Image pushed $(REGISTRY_LOCAL)/node-testing:$(VERSION)

run-gpu:
	$(APPTAINER) build node-testing.sif docker-daemon://node-testing:latest
	$(APPTAINER) run --nv --env SHORT_TEST=$(SHORT_TEST)  --mount type=bind,source=targets,destination=/targets --hostname localhost node-testing.sif

run:
	$(DOCKER) run -it --rm --env SHORT_TEST=$(SHORT_TEST)  -v $${PWD}/targets:/targets --hostname localhost node-testing:latest /bin/bash

replace-image:
	echo \\n The image in ${REGISTRY_NODE} will replace the one in ${BENCHMARK_DIRECTORY} through node ${RANDOM_NODE}, ok?
	read ok

	set -e
	$(DOCKER) pull ${REGISTRY_NODE}/node-testing:latest
	docker_id=`$(DOCKER) images ${REGISTRY_NODE}/node-testing:latest | tail -n 1 | awk '{print $$3}'`
	digest=`$(DOCKER) inspect $${docker_id} | jq '.[0].RepoDigests[0]'`
	$(LOCAL_APPTAINER) pull --force test.sif docker://${REGISTRY_NODE}/node-testing:latest
	scp test.sif root@$(RANDOM_NODE):$(APPTAINER_IMAGE)
	ssh root@$(RANDOM_NODE) "echo `date +%Y-%m-%dT%H:%M` $${digest} >> $(BENCHMARK_DIRECTORY)/updates.txt"
	echo "The updates file is now containing..."
	ssh root@$(RANDOM_NODE) "cat $(BENCHMARK_DIRECTORY)/updates.txt"


# Production related
deploy-target:
	echo \\n Updating target in $(BENCHMARK_DIRECTORY) and giving it to $(DIRECTORY_OWNER), ok?
	read ok

	TMP_FILE=`mktemp`
	echo '# This file should not be modified, please update on git and push it' > $${TMP_FILE}
	cat targets/targets.yaml >> $${TMP_FILE}
	ssh root@${ADMIN_NODE} "mkdir -p $(BENCHMARK_DIRECTORY) && chown $(DIRECTORY_OWNER):ID-HPC-ADMIN -R $(BENCHMARK_DIRECTORY) && chmod g+w -R $(BENCHMARK_DIRECTORY)"
	scp $${TMP_FILE} root@${ADMIN_NODE}:$(BENCHMARK_DIRECTORY)/targets.yaml

deploy-script:
	echo \\n Updating script in $(REMOTE_BIN_DIRECTORY) and giving it to $(DIRECTORY_OWNER), ok?
	read ok

	set -e
	echo "#!/bin/bash" > $(REMOTE_BIN_NAME)
	echo "export BENCHMARK_DIRECTORY=$(BENCHMARK_DIRECTORY)" >> $(REMOTE_BIN_NAME)
	echo "export APPTAINER_IMAGE=$(APPTAINER_IMAGE)" >> $(REMOTE_BIN_NAME)
	cat benchmark-node.in >> $(REMOTE_BIN_NAME)
	chmod a+x $(REMOTE_BIN_NAME)
	scp benchmark-node root@${ADMIN_NODE}:$(REMOTE_BIN_DIRECTORY)/

run-on-node:
	echo \\n Running on node $${NODES} using image from $(REGISTRY_NODE), ok?
	read ok

	clush --user root -f 8 -v -w $${NODES} "APPTAINER_BIND='/opt' APPTAINER_CACHEDIR=$(CACHE_DIR) $(APPTAINER) run --nv --rocm --env SHORT_TEST=$(SHORT_TEST) --mount type=bind,source=$(BENCHMARK_DIRECTORY),destination=/targets docker://$(REGISTRY_NODE)/node-testing:latest"

get-node-spec:
	clush -f 8 --user root -v -w $${NODES} "$(REMOTE_BIN_DIRECTORY)/$(REMOTE_BIN_NAME) --get-spec"

clean-cache:
	ssh ${RANDOM_NODE} 'APPTAINER_CACHEDIR=$(CACHE_DIR) $(APPTAINER) cache clean -f'

# Analysis
format-code:
	black .
	isort --profile black .
	flake8 .

download-results:
	mkdir -p data
	ssh ${ADMIN_NODE} "cd /cluster/spool/benchmarks/ && tar zcf data.tar.gz AMD* Intel* errors"
	scp root@${ADMIN_NODE}:/cluster/spool/benchmarks/data.tar.gz data/
	cd data && tar zxf data.tar.gz && rm data.tar.gz

find-failures:
	. venv/bin/activate
	python3 analysis/find_failure.py

analyze-data:
	. venv/bin/activate
	python3 analysis/analyze_data.py

analyze-data-install:
	python3 -m venv venv
	. venv/bin/activate
	pip3 install -r analysis/requirements.txt
